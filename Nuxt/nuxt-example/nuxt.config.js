import { resolve } from "path";

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target:'static', //deployment target same as ssr=false 
  // target: 'server', // deployment target same as ssr=true
  // ssr:true ,//display data in terminal and browser as ssr
    // ssr:false, //display data only browser not Nuxt ssr like display
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'nuxt-example',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      // {
      //   rel:'stylesheet',
      //   link:'https://fonts.googleapis.com/css?family=Roboto&display=swap'
      // }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/style.css',
    '~/assets/css/main',
    '~/assets/scss/variable.scss'
  ],

    //to change .nuxt file to nuxt-dist build directory 
    // add nuxt-dist file name to .gitignore file 
    // buildDir:'nuxt-dist',

  // styleResources:{
  //   scss:[
  //     '~/assets/scss/variable.scss'
  //   ]
  // },
  // ignore:'pages/Login.vue',
  // loading:{
  //   color:'blue',
  //   height:'5px'  ,
  //   failedColor:'red'
  // },

  loading:'~/components/Loading.vue',
  csp:true,
  // loading:false,
  // router: {
  //   prefetchLinks: false
  // },
  // router:{
  //   linkActiveClass: 'my-custom-active-link'
  // },
  // loading:'~/components/Loading.vue',
  // loading:false,

  // loading:{
  //   continuous:true,
  // },

  // loadingIndicator:{
  //   name:'~/components/Loading.vue',
  //   color:'#3B8070',
  //   background:'white'
  // },
  // loadingIndicator: {
  //   name: 'cube-grid',
  //   color: '#3B8070',
  //   background: 'white'
  // },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '~/plugins/preview.client.js',
    '~/plugins/vue-tooltip.js',
    '~/plugins/hello.js',
    '~/plugins/axios.js',
    // {src:'~/plugins/axios.js',mode:'client'}  ,
    // {src:'~/plugins/axios.js',mode:'server'}  
  ],
  // extendPlugins(plugins) {
  //   const pluginIndex = plugins.findIndex((src) => src === '~/plugins/axios.js'
  //   )
  //   const shouldBeFirstPlugin = plugins[pluginIndex]

  //   plugins.splice(pluginIndex, 1)
  //   plugins.unshift(shouldBeFirstPlugin)

  //   return plugins
  // },
  // Auto import components: https://go.nuxtjs.dev/config-components
  // components: true,
  // components:{
  //   dirs:[
  //     '~/components',
  //     // '~/components/base',
  //     {path:'~/components/base',extensions:['vue']}
  //   ]  
  // },

  components:[
    '~/components',
    {path:'~/components/base',extensions:['vue']}
  ],
  publicRuntimeConfig:{
    axios: {
      baseURL: 'https://api.nuxtjs.dev'
    },
    // baseURL:process.env.NUXT_ENV_BASE_URL || 'http://nuxtjs.org'
  },
  privateRuntimeConfig:{
    privatePassword:process.env.NUXT_ENV_Private
  },
  alias:{
    'images':resolve(__dirname,'./assets/Images')
  },
  // dir: {
  //   // Rename `pages` directory to `routes`
  //   pages: 'routes'
  // },
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  // buildModules: [
  //   '@nuxt/style-resources',
  // ],

  // Modules: https://go.nuxtjs.dev/config-modules
  // exampleMsg:'hello',
  modules: [
    '@nuxt/http',
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    // '~/modules/sample/module.js',
    ['~/modules/sample/module.js',{pathName:'products'}],
    // '@nuxt/content',
    '~/modules/examples',
    // ['~/modules/examples',{token:'123'}]
    // '@nuxtjs/router'
  ],
  axios: {
    proxy: true // Can be also an object with default options
  },
  content: {},
  styleResources:{
    scss:['./assets/scss/*.scss']
  },
  router: {
    // trailingSlash: true,
    linkActiveClass: 'my-custom-active-link',
    linkExactActiveClass: 'my-custom-exact-active-link',
    middleware: ['class','stats'],
   
    // extendRoutes(routes,resolve){
    //   routes.push({
    //     name:'customerror',
    //     path:'*',
    //     title:'Error',
    //     component:resolve(__dirname,'pages/404.vue')
    //   })
    // }
    // routeNameSplitter: '-'
  },
  // generate: {
  //   routes: [
  //     '/'
  //   ]
  // },
  // generate: {
  //   exclude: [
  //     /^\/sorry/ // path starts with /admin
  //   ]
  // },
  // router:{
  //   mode:'hash',

  // },
  // router: {
  //   routeNameSplitter: '/*'
  // },
  // router: {
  //   extendRoutes(routes, resolve) {
  //     routes.push({
  //       path: '/Post/:id',
  //       component:resolve(__dirname, 'pages/Post'),
  //       chunkNames: {
  //         modal: 'components/modal'
  //       }
  //     })
  //   }
  // },
  // router:{
  //     extendRouters(routes,resolve){
  //         routes.push({
  //             path:'*',
  //             component:resolve(__dirname,'pages/404.vue')
  //         })
  //   }
  // },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  // build: {
  //   filenames: {
  //     chunk: ({ isDev }) => (isDev ? '[name].js' : '[id].[contenthash].js')
  //   }
  // },
  // serverMiddleware: ['~/api/index'],
  cli: {
    badgeMessages: ['Hello World!'],
    bannerColor: 'blueBright' //Nuxt title color
  },
  server: {
    timing: {
      total: true
    }
  }
}
