import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'

function removeQueryParams(to) {
  if (Object.keys(to.query).length)
    return { path: to.path, query: {}, hash: to.hash }
}
const router = createRouter({
  history: createWebHistory(),
  // history:createWebHashHistory(),
  
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "Home" */ '../views/HomeView.vue'),
      meta: { transition: 'slideleft' },
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "About" */ '../views/AboutView.vue'),
      meta: { transition: 'slideright' },
    },
    {
      path:'/fetchbeforenav',
      name:'fetchbeforenav',
      component:() => import('../views/FetchBeforeNav.vue'),
      
    },
    {
      path:'/compuserouter',
      name:'compuserouter',
      component:() => import('../views/CompUseRouter.vue'),
     
    },
    {
      path:'/details/:id',
      name:'details',
      component:() => import('../views/UseRouterChild.vue'),
      props:true

    },
    {
      path:'/users/:id',
      component: () => import('../views/Users.vue'),
      name:'users',
      sensitive:true
    },
    {
      path:'/fetchusers/:postid',
      name:'fetchusers',
      component: () => import('../views/FetchUsers.vue')
    },
    {
      path:'/:pathMatch(.*)*',
      component:() => import('../views/NotFound.vue'),
    },
    // { path: '/user-:afterUser(.*)', component: UserGeneric },
    // {
    //   path:'/usersNum/:id(\\d+)',
    //   component: () => import('../views/Users.vue')
    // },
    // {
    //   path:'/users/:user+/:matchMultiParam',
    //   component: () => import('../views/Users.vue')
    // },
    // {
    //   path:'/nestedRoute',
    //   component: () => import('../views/Users.vue'),
    //   children:[
    //     {
    //       path:':id',
    //       component: () => import('../views/Users.vue')
    //     }
    //   ]
    // },
    // {
    //   path:'/home',
    //   redirect:'/'
    // },
    // {
    //   path:'/xyz',
    //   redirect:{name:'about'}
    // },
    // {
    //   path:'/search/:id/posts',
    //   redirect:to => {
    //     return 'profile'
    
    //   }
    // },
    // {
    //   path:'/users/:id',
    //   component: () => import('../views/Users.vue'),
    //   name:'users',
    //   sensitive:true,
    //   props:true,
    // },
    // {
    //   path:'/users/:id',
    //   components:{
    //     default: () => import('../views/Users.vue'),
    //     sidebar: () => import('../views/FetchUsers.vue')
    //   },
    //   props:{default:true,sidebar:true},
    // },
   
    // {
    //   path:'/',
    //   component: () => import('../views/HomeView.vue'),
    //   alias:'/home'
    // },
    {
      path:'/about',
      component:() => import('../views/AboutView.vue'),
      alias:'/aboutus',
      // beforeEnter:(to,from)=>{
      //   if(to.meta.title  && !window.user){
      //     return {name:'login'}
      //   }
      // },
      // meta: {title: 'About'}
      beforeEnter:[removeQueryParams]
    },
    {
      path:'/searchuser',
      component:() => import('../views/SearchUser.vue'),
      props:route => ({query:route.query.q})
    },
    // {
    //   path:'/setting',
    //   component:UserSetting,
    //   children:[
    //     {path:'email',component:() => import('../views/UserEmailSub.vue'),alias:'emailid',meta:{requiresAuth:false}}
    //   ]
    // } ,
    // {
    //   path:'/',
    //   components:{
    //     default:() => import('../views/LeftSide.vue'),
    //     () => import('../views/HomeView.vue'),
    //     () => import('../views/RightSide.vue')
    //   }
    // },
    // {
    //   path:'/rightside',
    //   components:{
    //     default:() => import('../views/RightSide.vue'),
    //     () => import('../views/HomeView.vue'),
    //     () => import('../views/LeftSide.vue')
    //   }
    // },
    {
      path:'/setting',
      component: () => import('../views/UserSetting.vue'),
      children:[
        {path:'email',component:() => import('../views/UserEmailSub.vue'),meta:{requiresAuth:true}},
        {path:'profile',components:{
          default:() => import('../views/UserProfileSub.vue'),
          setting:() => import('../views/UserProfilePre.vue')
        },meta:{requiresAuth:false}
        } 
      ]
    },
    {
      path:'/protected',
      name:'protected',
      component:() => import('../views/Protected.vue'),
      meta:{
        requiresAuth:true,
      }
    },
    {
      path:'/login',
      name:'login',
      component:() => import('../views/Login.vue')
    },
    // {
    //   path:'/nestedusers/:username',
    //   component:WatchUsers,
    //   name:'nestedusers',
    //   children:[
    //       {
    //         path:'',component:UserHome,name:'Home'
    //       },
    //       {
    //         path:'profile',component:UserProfiles,name:'profile'
    //       },
    //       {
    //         path:'posts',component:UserPost,name:'posts'
    //       }
    //   ]
    // }
  ],
  scrollBehavior:(to,from,savePosition) => {
    return {top:null ,savePosition:null,behavior:'smooth'}
  },
  strict:true,
})
//every route change in application this function run
router.beforeEach((to,from) => {
  if(to.meta.requiresAuth && !window.user){
      return {name:'login'}
  }
  // return false // cancle current naviagtion

  //if user not login then only redirect to login page and not goto any other router after login redirect to all routes
  // if(!window.user && to.name != 'login'){
  //   return {name:'login'}
  // }
})

// router.beforeResolve(async to => {
//   if (to.meta.requiresAuth) {
//     try {
//         console.log('try')
//     } catch (error) {
//       if (error) {
//         return false
//       } else {
//         throw error
//       }
//     }
//   }
// })

// router.afterEach((to,from,failure) => {
 
//     console.log(to.fullPath)
  
 
// })
export default router
