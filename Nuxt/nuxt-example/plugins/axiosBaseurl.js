import axios from "axios";
export default axios.create({
    baseURL:process.env.NUXT_ENV_baseurl,
    timeout:1000,
    headers:{'X-Custom-Header':'footer'}
})