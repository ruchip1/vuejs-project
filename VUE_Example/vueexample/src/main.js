import { createApp, createSSRApp } from 'vue'
import App from './App.vue'
import i18nPlugin from './plugins/i18n.js'
import './assets/main.css'

const app = createApp(App)

app.use(i18nPlugin,{
    greeting:{
        hello:'Bonjour!'
    }
})
app.directive('theme',{
    mounted(el,binding) {
        if(binding.value === 'primary'){
            el.style.backgroundColor = 'blue'
            el.style.color='white'
        }
        else if(binding.value === 'secondary'){
            el.style.backgroundColor = 'green',
            el.style.color='white'
        }
        else{
            el.style.backgroundColor = 'black'
        }
    }
})
app.mount('#app')
