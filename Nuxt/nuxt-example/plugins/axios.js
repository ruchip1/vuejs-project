// export default function ({$axios,redirect}){
//     console.log('axios');
//     $axios.onError(error => {
//         if(error.response.status === 404){
//             redirect('/sorry/')
//         }
//     })
// }
export default ({ $axios }) => {
	const url = 'https://api.nuxtjs.dev'
	const token = process.env.TOKEN 

	$axios.setBaseURL(url)
  
	if (token) $axios.setHeader('Authorization', token)
}
