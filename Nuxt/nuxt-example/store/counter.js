import axios from "axios"

export const state = () => ({
    counter: 0,
    authenticated:false,
    authentication:false,
    announcement: []
  })

export const mutations = {
  SetAuthentication(state,payload){
    state.authentication = payload
  },
  increment(state) {
    state.counter++
  },
  SET_ANNOUNCEMENT(state, title){
    state.announcement = title
  }
}
export const getters = {
  announce: state => state.announcement
};
export const actions = {
  async nuxtServerInit({ commit }) {
    const  res  = await axios('https://jsonplaceholder.typicode.com/posts')
    const data = res.data;
    const title = data.map(({title}) => title)
    store.commit('SET_ANNOUNCEMENT', title)
}
}
