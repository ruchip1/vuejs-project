import {createStore} from 'vuex'
import {decrement} from '../components/mutationType.js'
import { mutations } from '../components/mutations.js'
import createPersistedState from "vuex-persistedstate";
const incrementalPlugin = (store) => {
    store.subscribe((mutations,state) => {
        if(mutations.type == 'showCount'){
            state.Plugincount++
        }
    })
}
// Testing

// const getters = {
//     filteredProducts (state, { filterCategory }) {
//         return state.products.filter(product => {
//             return product.category === filterCategory
//         })
//     }
// }

// mocha.setup('bdd');
// let assert = chai.assert;
// let expect = chai.expect;
// const { incrementss } = mutations
// describe('mutations', () => {
//     it('INCREMENT', () => {
//         const state = { count: 0 }
//         incrementss(state)
//         expect(store.state.count).to.equal(1)
//     })
// })
// describe('getters', () => {
//     it('filteredProducts', () => {
//         const state = {
//             products: [
//                 { id: 1, title: 'Apple', category: 'fruit' },
//                 { id: 2, title: 'Orange', category: 'fruit' },
//                 { id: 3, title: 'Carrot', category: 'vegetable' }
//             ]        
//         }
        
//         const filterCategory = 'fruit'
        
//         const result = getters.filteredProducts(state, { filterCategory })
        
//         expect(result).to.deep.equal([
//             { id: 1, title: 'Apple', category: 'fruit' },
//             { id: 2, title: 'Orange', category: 'fruit' }
//         ])
//     })
// })
// mocha.run();
const moduleA = {
    namespaced: true,
    state:{
        count:5
    },
    mutations:{
        increment(state){
            state.count++
        }
    },
    getters:{
        doubleCount(state){
            return state.count * 2
        }
    },
    actions:{
            incrementIfOdd({state,commit}){
                if(state.count % 2 === 1){
                    commit('increment')
                }
            }
    }

}
const moduleB = {
    namespaced: true,
    modules:{
        subModule:{
            namespaced:true,
            state:{
                subModuleCount:3,
            },
            mutations:{
                increments (state){
                    state.subModuleCount ++
                }
            },
            getters:{
                
            },
            actions:{
    
            }
        }
    },
    state:{
        count:8,
        products:[],
        moduleBtodos:[
            {id:1,text:'learning vuex',done:true},
            {id:2,text:'learning vue-router',done:false},
            {id:3,text:'learning vue js',done:true},
            {id:4,text:'learning react js',done:false}
        ]
    },
    mutations:{
        increment(state){
            state.count++
        }
    },
    getters:{
        moduleBdoneTodos:state => {
            return state.moduleBtodos.filter(modlueBtodo => modlueBtodo.done)
        },
        displayGlobalState :(state,getters,rootState ,rootGetters) => {
            return  rootGetters.doneTodosCount
            // return rootState.products
            // state.products = rootState.products
            // console.log(state.products)
            // console.log(rootGetters.doneTodosCount)
            // console.log(rootState.products)
        } ,
        
    },
    actions:{
        incrementByAsync(context){
            context.commit('increment')
        },
        doSomeGlobalChanges :({dispatch,commit,getters,rootGetters}) => {
            commit('decrement',null,{ root: true })
        }
    }

}
// const myPluginWithSnapshot = (store) => {
//     let prevState = _.cloneDeep(store.state)
//     store.subscribe((mutation, state) => {
//       let nextState = _.cloneDeep(state)
  
//       // compare `prevState` and `nextState`...
  
//       // save state for next mutation
//       prevState = nextState
//     })
//   }
const store = createStore({
    // plugins: process.env.NODE_ENV !== 'production'
    // ? [myPluginWithSnapshot]
    // : [],
    //used only development mode not enable in production model
    // strict:process.env.NODE_ENV != 'production',
    // strict:true, 
    // modules:{
    //     moduleB:moduleB,
    //     moduleA:moduleA
    // },
    state:{
        
            count:1,
            countPayload:0,
            Plugincount : 0,
            message:'Hello Ruchi',
            anotherMessage:'Hello From Ruchi PAtel',
            products:[
                {id:1,name:'Chill',price:240},
                {id:2,name:'Termaric',price:250},
                {id:3,name:'Oragano',price:30}
            ],
            todos:[
                {id:1,text:'learning vuex',done:true},
                {id:2,text:'learning vue-router',done:false},
                {id:3,text:'learning vue js',done:true},
                {id:4,text:'learning react js',done:false}
            ]
        
    },
    mutations:{
        // increment (state){
        //     state.count++
        // },
        showCount(state){
            console.log(state.Plugincount)
        },
        incrementss (state){
            state.count++
        },
        incrementBy (state,n) {
            state.count += n
        },
        incrementByUsingPayload (state,payload) {
            state.countPayload += payload.amount
        },
        [decrement] (state) {
            state.count --
        },
        multiplication (state){
            state.count *= 2
        },
        updateMessage (state,message) {
            state.message = message
        },
        updateanotherMessage (state,message){
            state.anotherMessage = message
        }
    },
    getters:{
        doneTodos:state => {
            return state.todos.filter(todo => todo.done)
        },
        doneTodosCount:(state,getters) => {
            return getters.doneTodos.length
        },
        getTodoById:(state) => (id) => {
            return state.todos.find(todo => todo.id === id)
        }
    },
    actions:{
        incrementAsync(context){
           setTimeout(() => {
            context.commit('increment')
           }, 1000);
        },
        incrementByAsync(context,payload){
            context.commit('incrementBy',payload)
        },
        decrementByAsync(context){
            return new Promise((reslove,reject) => {
                setTimeout(() => {
                    context.commit('decrement')
                    console.log('completed decrement')
                    reslove()
                }, 1000);
            })
        },
        // multiplicationAsync({dispatch,commit}){
        //         commit('multiplication')
        // },
        async multiplicationAsync({dispatch,commit}){
            return await dispatch('decrementByAsync').then(() => {
                setTimeout(() => {
                    commit('multiplication')
                    console.log('completed multiplication')
                }, 1000);
            })
    },
    },
    plugins:[incrementalPlugin,createPersistedState()]

})
store.registerModule('moduleA',moduleA)
store.registerModule('moduleB',moduleB)
export default store;