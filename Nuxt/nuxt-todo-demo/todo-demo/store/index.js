export const state = () => ({
  tasks: [],
});
export const mutations = {
  addTask(state, newtask) {
    state.tasks = [...state.tasks, newtask];
  },
  toggleReminder(state, id) {
    state.tasks = state.tasks.map((task) =>
      task.id === id ? { ...task, reminder: !task.reminder } : task
    );
  },
  removeTask(state, id) {
    state.tasks = state.tasks.filter((task) => task.id !== id);
  },
};
