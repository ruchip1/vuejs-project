import axios from "axios"

export const state = () => ({
    titles: []
  })

export const mutations = {
  SET_ANNOUNCEMENT(state, title){
    state.titles = title
  }
}
export const getters = {
  announce: state => state.titles
};
export const actions = {
  async nuxtServerInit({ commit }) {
    const  res  = await axios('https://jsonplaceholder.typicode.com/posts')
    const data = res.data;
    commit('SET_ANNOUNCEMENT', data)
    }
}
