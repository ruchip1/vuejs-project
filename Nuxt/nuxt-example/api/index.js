// var express = require('express')
// var app = express()
// app.get('/',function(req,res) {
//     res.send('helllo Workd')
//     console.log('run serverMiddleware');
// });
// module.exports = {
//     path:'/api/',
//     handler:app,
// }
export default function (req, res, next) {
    // req is the Node.js http request object
    console.log(req.url,'server Middleware')
  
    // res is the Node.js http response object
  
    // next is a function to call to invoke the next middleware
    // Don't forget to call next at the end if your middleware is not an endpoint!
    next()
  }