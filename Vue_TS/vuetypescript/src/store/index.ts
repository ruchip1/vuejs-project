import { InjectionKey } from 'vue'
import { createStore, Store } from 'vuex'
export interface State{
  todos:string[],
}
export const key: InjectionKey<Store<State>> = Symbol()
export const store = createStore<State>({
  state: {
    todos:['Learned Vue js','Learning Typescript']
  },
  getters: {
  },
  mutations: {
    ADD_TODO (state,todo:string){
      state.todos.push(todo)
    },
    DELETE_TODO (state,todo:string){
      state.todos = state.todos.filter(t => t != todo)
    }
  },
  actions: {
    addTodo ({commit},todo:string){
      commit('ADD_TODO',todo)
    },
    deleteTodo({commit},todo:string){
      commit("DELETE_TODO",todo)
    }
  },
  modules: {
  }
})
