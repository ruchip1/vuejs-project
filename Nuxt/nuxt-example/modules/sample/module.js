const consoleObjKey = (obj) => {
    for (const [key,value] of Object.entries(obj)){
        console.log(`${key} : ${value}`);
    }
}
export default function(moduleOptions){
    // consoleObjKey(this)
    console.log(moduleOptions);
    this.extendRoutes(routes => {
        routes.push({
            name:'items',
            // path:'/items',
            path:`${moduleOptions.pathName}`,
            component:'~/modules/sample/pages/items.vue'
        })
        routes.push({
            name:'item',
            // path:'/items/:id',
            path:`${moduleOptions.pathName}/:id`,
            component:'~/modules/sample/pages/_id.vue'
        })
    })

    this.addPlugins({
        src:path.resolve(__dirname,'plugin.js'),
        options:{
            pathName : moduleOptions.pathName
        }
    })
}