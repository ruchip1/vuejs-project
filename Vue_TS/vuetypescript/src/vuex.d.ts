import { Store } from "vuex";
declare module '@vue/runtime-core'{
    interface State{
        count:number
    }
    //provide typing for 'this.$store
    interface ComponentCustomProperties{
        $store:Store<State>
    }
}