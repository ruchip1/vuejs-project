import { createSSRApp } from 'vue'

export function createApp() {
  return createSSRApp({
    data: () => ({ count: 2 }),
    template: `<button @click="count++">{{ count }}</button>`
  })
}