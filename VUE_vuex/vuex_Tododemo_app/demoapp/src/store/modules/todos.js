import axios from "axios";
const state = {
    todos:[]
};

const getters = {
    allTodos : state => state.todos
} ;
 
const actions = {
    //fetch todo api
    async fetchTodos({commit}) {
        const res = await axios.get('https://jsonplaceholder.typicode.com/todos')
        commit('setTodos',res.data)
        console.log(res.data)
    },

    //add todo using post request
    async addTodo({commit},title){
        const res = await axios.post('https://jsonplaceholder.typicode.com/todos',{ 
            title, completed:false
        })
        commit('newTodo',res.data)
    },

    //delete todo
    async deleteTodo({commit},id){
        await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
        commit('removeTodo',id)
    },

    //filter todo 
    async filterTodos({ commit } ,e){
        const limit = parseInt(e.target.options[e.target.selectedIndex].innerText)
        const res = await axios.get(`https://jsonplaceholder.typicode.com/todos?_limit=${limit}`)
        commit('setTodos',res.data)
        console.log(limit)
    },

    //update todo 
    async updateTodo({ commit },updateTodo) {
        const res = await axios.put(`https://jsonplaceholder.typicode.com/todos/${updateTodo.id}`,updateTodo)
        console.log(res.data)
        commit('updateTodo',res.data)
    }
};

const mutations = {
    setTodos : (state,todos) => (state.todos = todos),
    newTodo : (state,todo) => state.todos.unshift(todo),
    removeTodo : (state,id) => { state.todos = state.todos.filter(todo => todo.id !== id)},
    updateTodo : (state,updateTodo) => {
        const index = state.todos.findIndex(todo => todo.id === updateTodo.id)
        if(index != -1){
             state.todos.splice(index,1,updateTodo)
        }
    }
} ;

export default {
    state,
    getters,
    actions,
    mutations,
    
}