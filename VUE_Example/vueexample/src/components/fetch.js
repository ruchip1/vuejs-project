import { isRef, ref, unref, watchEffect } from "vue"

export function useFetch(url){
    // const data = ref(null)
    // const error = ref(null)
    // fetch(url).then((res) => res.json()).then((json) => (data.value = json)).catch((err) => error.value = err)
    // return {data,error}

    const data = ref(null)
    const error = ref(null)
   async function  dofetch(){
        data.value = null
        error.value = null
        const urlValue = unref(url)
        try{
            const res = await fetch(urlValue)
            data.value = await res.json()
        }catch(e){
            error.value = e
        }
    }
    if(isRef(url)){
        watchEffect(dofetch)
    }
    else{
        dofetch()
    }
    return {data ,error}
}

export function useFetchA(val){
    const valueA = ref(val)
    console.log(valueA.value)
    return {valueA}
}